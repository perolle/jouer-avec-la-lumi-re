const BASE_URL = 'http://192.168.1.62:8080/api/v1/devices/';
const h = 0;
const s = '100';
const l = '100';

function toggle(mac){
    fetch(BASE_URL + mac,
        {
            method: 'POST', 
            headers:{'content-Type': 'application/json',},
            //mode: 'cors',//cross origine resource sharing pour verifier les autorisatrions
            body: JSON.stringify({
                action: 'toggle',
                ramp: 50,
            })
        }
    )//{}passe un objet en paramettre
    
}

function setColor(mac, color) {
    fetch(BASE_URL + mac,
        {
            method: 'POST', 
            headers:{'content-Type': 'application/json',},
            //mode: 'cors',//cross origine resource sharing pour verifier les autorisatrions
            body: JSON.stringify({
                color: color,
                ramp: 50,
            })
        }
    )
}

fetch(BASE_URL)
    .then(resp => resp.json())
    .then(body => { //resultat de requête httpbody
        const bulbList = document.querySelector('#bulbs');
        for(let bulb of body){
            let htmlBulb = document.createElement('li');
            let onoffBulb = document.createElement('input');
            onoffBulb.type= 'button';
            let range = document.createElement('input')
            range.type='range';
            range.min = 0;
            range.max = 355;
            onoffBulb.addEventListener('click', () => {
                onoff(onoffBulb)
                toggle(bulb.mac)
            });
            onoffBulb.id = bulb.mac;
            //range.id = 'range'+bulb.mac;
            htmlBulb.innerText = bulb.mac;
            bulbList.appendChild(htmlBulb);
            bulbList.appendChild(onoffBulb);
            bulbList.appendChild(range);
            range.addEventListener('input', () => {
                let color = range.value + ';100;100';
                setColor(bulb.mac, color);
            });
            onoffBulb.value = onoff(bulb.mac);
            //htmlBulb.addEventListener('click', ()=> toggle(bulb.mac))
        }   
    })
    function onoff(button){
        currentvalue = button.value;
        if(currentvalue == "off"){
          button.value="on";
          return "on";
        }else{
          button.value="off";
          return "off";
        }
      }
   

 
/*

    function onSliderChanged(mac) {
        let r = document.getElementById("red").value;
        let g = document.getElementById("green").value;
        let b = document.getElementById("blue").value;
        document.color = "#"+dh(r)+dh(g)+dh(b);
        document.getElementById(mac).innerHTML="rgb("+r+","+g+","+b+")="+document.color;
    }
    
    function dh(d) { // decimal to hex conversion
        let hex = Number(d).toString(16);
        if (hex.length < 2) hex = "0" + hex;
        return hex;
    }
*/
